<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<NamedLayer>
	<Name>Two color gradient</Name>
	<UserStyle>
		<Title>SLD Cook Book: Two color gradient</Title>
		<FeatureTypeStyle>
			<Rule>
              <MinScaleDenominator>10001</MinScaleDenominator>
          	  <MaxScaleDenominator>25000001</MaxScaleDenominator>
				<RasterSymbolizer>
					<Opacity>0.5</Opacity>
				</RasterSymbolizer>
			</Rule>
		</FeatureTypeStyle>
	</UserStyle>
</NamedLayer>
</StyledLayerDescriptor>
