# SLD skrár Landmælinga Íslands README
Starfsmenn Landmælinga Íslands eru stoltir af því að hafa opin gögn. Til þess að auðvelda nýtingu gagnanna viljum við einnig deila svokölluðum SLD skrám sem notaðir eru til að stýra útliti gagnanna samkvæmt stöðlum OGC (Open Geospatial Consortium) http://www.opengeospatial.org/ 

# QGIS
Það eru ýmsir hugbúnaðir sem ráða við SLD skrár og þar á meðal er opni hugbúnaðurinn QGIS. Hugbúnaðinn má nálgast hér http://qgis.org/en/site/