<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>IS_50V_örnefni_punkar_label</se:Name>
    <UserStyle>
      <se:Name>ornefni_punktar</se:Name>
      <se:FeatureTypeStyle>
       <se:Rule>
          <se:Name></se:Name>
          <se:Description>
            <se:Title>IS_50V_örnefni_punkar_label</se:Title>
          </se:Description>
		  <se:MaxScaleDenominator>200001</se:MaxScaleDenominator>
		  <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>square</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#000000</se:SvgParameter>
                  <se:SvgParameter name="fill-opacity">0.80</se:SvgParameter>
                </se:Fill>
                <se:Stroke/>
              </se:Mark>
              <se:Size>0.5</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
		   <se:TextSymbolizer>
                        <se:Label>
                         <ogc:PropertyName>nafnfitju</ogc:PropertyName>
						</se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Verdana</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
							<se:SvgParameter name="font-style">italic</se:SvgParameter>
                            <se:SvgParameter name="font-size">9</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                              <se:AnchorPoint>
  								<se:AnchorPointX>0.0</se:AnchorPointX>
  								<se:AnchorPointY>0.0</se:AnchorPointY>
							  </se:AnchorPoint> 
							  <se:Displacement>
							  <se:DisplacementX>4</se:DisplacementX>
							  <se:DisplacementY>0</se:DisplacementY>
							 </se:Displacement>							  
                          	</se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">10</se:VendorOption>
						<!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
						<se:VendorOption name="partials">true</se:VendorOption>
           </se:TextSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>