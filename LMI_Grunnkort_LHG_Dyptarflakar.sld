<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>dypgarflakar</Name>
    <UserStyle>
      <Title>dyptarflakar_lhg_uppf</Title>
      <FeatureTypeStyle>
        <Rule>
          <Name>1</Name>            
          <Title>2500-3000m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>1</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#33CCFF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#33CCFF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>2</Name>
          <Title>2000-2500m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>2</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#3DCFFF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#3DCFFF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>3</Name>
          <Title>1500-2000m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>3</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#47D1FF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#47D1FF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>4</Name>
          <Title>1000-1500m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>4</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#52D4FF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#52D4FF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>5</Name>
          <Title>500-1000m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>5</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#5CD6FF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#5CD6FF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>6</Name>
          <Title>400-500m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>6</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#66D9FF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#66D9FF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>7</Name>
          <Title>300-400m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>7</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#70DBFF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#70DBFF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>8</Name>
          <Title>200-300m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>8</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#7ADEFF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#7ADEFF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>9</Name>
          <Title>150-200m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>9</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#85E0FF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#85E0FF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>10</Name>
          <Title>100-150m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>10</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#8FE3FF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#8FE3FF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>11</Name>
          <Title>75-100m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>11</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#99E6FF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#99E6FF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>12</Name>
          <Title>50-75m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>12</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#A3E8FF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#A3E8FF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>13</Name>
          <Title>20-50m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>13</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#ADEBFF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#ADEBFF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name>14</Name>
          <Title>0-20m</Title>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>level</ogc:PropertyName>
              <ogc:Literal>14</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#B8EDFF</CssParameter>
      </Fill>
      <Stroke>
        <CssParameter name="stroke">#B8EDFF</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
      </Stroke>
          </PolygonSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
