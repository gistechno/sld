<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld  StyledLayerDescriptor.xsd">
    <NamedLayer>
        <Name>Simple point with stroke</Name>
        <UserStyle>
            <Title>Fjarskiptamannvirki</Title>
            <FeatureTypeStyle>
                <Rule>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>mannvirki</ogc:PropertyName>
                            <ogc:Literal>6500</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <MinScaleDenominator>1000</MinScaleDenominator>
                    <MaxScaleDenominator>100000</MaxScaleDenominator>
                    <PointSymbolizer>
                        <Graphic>
                            <Mark>
                                <!--WellKnownName>ttf://Webdings#0x0064</WellKnownName-->
                              	<WellKnownName>ttf://Wingdings#0x00A4</WellKnownName>
                                <Fill>
                                    <CssParameter name="fill">#000000</CssParameter>
                                  	<CssParameter name="fill-opacity">0.3</CssParameter>
                                </Fill>
                                <Stroke>
                                  	<CssParameter name="stroke">#000000</CssParameter>
                                  	<CssParameter name="stroke-opacity">0.3</CssParameter>
                                </Stroke>  
                            </Mark>
                            <Size>15</Size>
                        </Graphic>
                    </PointSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>