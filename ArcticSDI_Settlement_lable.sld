<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>mannvirki_flakar</Name>
    <UserStyle>
      <Name>mannvirki_flakar</Name>
      <FeatureTypeStyle>
        <Rule>
          <Name> 41 - 50000 </Name>
            <Title> 41 - 50000 </Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>ibuarfj_16</ogc:PropertyName>
                <ogc:Literal>41</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>ibuarfj_16</ogc:PropertyName>
                <ogc:Literal>50000</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#464646</CssParameter>
                        </Font>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
                        <VendorOption name="polygonAlign">ortho</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
                    </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>50k to 100k</Name>
            <Title>50k to 100k</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>ibuarfj_16</ogc:PropertyName>
                <ogc:Literal>50000</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>ibuarfj_16</ogc:PropertyName>
                <ogc:Literal>100000</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">8.5</CssParameter>
                            <CssParameter name="font-color">#464646</CssParameter>
                        </Font>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
                        <VendorOption name="polygonAlign">ortho</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
                    </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>100k to 500k</Name>
            <Title>100k to 500k</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>ibuarfj_16</ogc:PropertyName>
                <ogc:Literal>100000</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>ibuarfj_16</ogc:PropertyName>
                <ogc:Literal>121486</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">10</CssParameter>
                            <CssParameter name="font-color">#464646</CssParameter>
                        </Font>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
                        <VendorOption name="polygonAlign">ortho</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
                    </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>