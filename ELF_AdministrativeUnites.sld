<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
		xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
		xmlns="http://www.opengis.net/sld" 
		xmlns:ogc="http://www.opengis.net/ogc" 
		xmlns:xlink="http://www.w3.org/1999/xlink" 
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<!-- a named layer is the basic building block of an sld document -->
  <NamedLayer>
    <Name>AdministrativeUnit</Name>
    <UserStyle>
      <Name>AdministrativeUnit</Name>
      <FeatureTypeStyle>
        <Rule>
          <Name></Name>
          <MinScaleDenominator>100</MinScaleDenominator>
          <MaxScaleDenominator>2000</MaxScaleDenominator>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#ffff66</CssParameter>
              <CssParameter name="fill-opacity">0.50</CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#ff0033</CssParameter>
              <CssParameter name="stroke-width">4</CssParameter>
              <CssParameter name="stroke-linejoin">bevel</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name></Name>
          <MinScaleDenominator>2000</MinScaleDenominator>
          <MaxScaleDenominator>10000</MaxScaleDenominator>
		  <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>sveitarfelag</ogc:PropertyName>
                        </Label>
					   <Font>
                            <CssParameter  name="font-family">Arial</CssParameter >
                            <CssParameter  name="font-size">20.00</CssParameter >
                        </Font>
						<Fill>
							<CssParameter name="fill">#ff0033</CssParameter>
						</Fill>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="polygonAlign">mbr</VendorOption>
						 <VendorOption name="maxDisplacement ">250</VendorOption>
          </TextSymbolizer>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#ffff66</CssParameter>
              <CssParameter name="fill-opacity">0.50</CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#ff0033</CssParameter>
              <CssParameter name="stroke-width">4</CssParameter>
              <CssParameter name="stroke-linejoin">bevel</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Name></Name>
          <MinScaleDenominator>10000</MinScaleDenominator>
          <MaxScaleDenominator>500000</MaxScaleDenominator>
		  <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>sveitarfelag</ogc:PropertyName>
                        </Label>
					   <Font>
                            <CssParameter  name="font-family">Arial</CssParameter >
                            <CssParameter  name="font-size">12</CssParameter >
                        </Font>
						<Fill>
							<CssParameter name="fill">#ff0033</CssParameter>
						</Fill>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="polygonAlign">mbr</VendorOption>
						 <VendorOption name="maxDisplacement ">250</VendorOption>
          </TextSymbolizer>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#ffff66</CssParameter>
              <CssParameter name="fill-opacity">0.50</CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#ff0033</CssParameter>
              <CssParameter name="stroke-width">4</CssParameter>
              <CssParameter name="stroke-linejoin">bevel</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
