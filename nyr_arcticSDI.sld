<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>soilsurfaceregions</Name>
        <UserStyle>
            <Name>soilsurfaceregions</Name>
            <FeatureTypeStyle>
                <Rule>
                    <Name>Sand</Name>
                    <Title>Sand</Title>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>code_12</ogc:PropertyName>
                            <ogc:Literal>331</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <MaxScaleDenominator>500001</MaxScaleDenominator>
                       <PolygonSymbolizer>
                        <Fill>
                            <CssParameter name="fill">#E0E0E0</CssParameter>
                        </Fill>
                    </PolygonSymbolizer>
					<PolygonSymbolizer>
                        <Fill>
                            <GraphicFill>
                                <Graphic>
                                    <Mark>
										<WellKnownName>shape://dot</WellKnownName>
										<Stroke>
											<CssParameter name="stroke">#969696</CssParameter>
										</Stroke>
									</Mark>
									<Size>5</Size>
                                </Graphic>
                            </GraphicFill>
                        </Fill>
                    </PolygonSymbolizer>
                </Rule>
                <Rule>
                    <Name>Rocky region</Name>
                    <Title>Rocky region</Title>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>code_12</ogc:PropertyName>
                            <ogc:Literal>332</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <MaxScaleDenominator>500001</MaxScaleDenominator>
                    <PolygonSymbolizer>
                        <Fill>
                            <CssParameter name="fill">#e0e0e0</CssParameter>
                        </Fill>
                    </PolygonSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>