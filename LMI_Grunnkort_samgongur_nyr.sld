<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>samgongur_linur</Name>
    <UserStyle>
      <Name>samgongur_linur</Name>
      <FeatureTypeStyle>
        <Rule>
          <Name>Bundið slitlag 5m</Name>
            <Title>Bundið slitlag 5m</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:And>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortbirta</ogc:PropertyName>
                  <ogc:Literal>3000000</ogc:Literal>
                </ogc:PropertyIsEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                  <ogc:Literal>Bundið slitlag</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>einingvegakerfis</ogc:PropertyName>
                <ogc:Literal>1</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>1000001</MinScaleDenominator>
          <MaxScaleDenominator>5000001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#e60000</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
		<Rule>
          <Name>Bundið slitlag göng 5m</Name>
            <Title>Bundið slitlag göng 5m</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:And>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortbirta</ogc:PropertyName>
                  <ogc:Literal>3000000</ogc:Literal>
                </ogc:PropertyIsEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                  <ogc:Literal>Bundið slitlag</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>einingvegakerfis</ogc:PropertyName>
                <ogc:Literal>4</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>1000001</MinScaleDenominator>
          <MaxScaleDenominator>5000001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#e60000</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
			  <CssParameter name="stroke-dasharray">3 2</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
		
        <Rule>
          <Name>Malarvegur 5m</Name>
            <Title>Malarvegur 5m</Title>
           <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortbirta</ogc:PropertyName>
                <ogc:Literal>3000000</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                <ogc:Literal>Malarvegur</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>1000001</MinScaleDenominator>
          <MaxScaleDenominator>5000001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#a87000</CssParameter>
              <CssParameter name="stroke-width">0.2</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Bundið slitlag 1m</Name>
            <Title>Bundið slitlag 1m</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:And>
                <ogc:PropertyIsGreaterThanOrEqualTo>
                  <ogc:PropertyName>kortbirta</ogc:PropertyName>
                  <ogc:Literal>1000000</ogc:Literal>
                </ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                  <ogc:Literal>Bundið slitlag</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>einingvegakerfis</ogc:PropertyName>
                <ogc:Literal>1</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>500001</MinScaleDenominator>
          <MaxScaleDenominator>1000001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#e60000</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
		<Rule>
          <Name>Bundið slitlag göng 1m</Name>
            <Title>Bundið slitlag göng 1m</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:And>
                <ogc:PropertyIsGreaterThanOrEqualTo>
                  <ogc:PropertyName>kortbirta</ogc:PropertyName>
                  <ogc:Literal>1000000</ogc:Literal>
                </ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                  <ogc:Literal>Bundið slitlag</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>einingvegakerfis</ogc:PropertyName>
                <ogc:Literal>4</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>500001</MinScaleDenominator>
          <MaxScaleDenominator>1000001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#e60000</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
			  <CssParameter name="stroke-dasharray">5 3</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Malarvegur 1m</Name>
            <Title>Malarvegur 1m</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kortbirta</ogc:PropertyName>
                <ogc:Literal>1000000</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                <ogc:Literal>Malarvegur</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>500001</MinScaleDenominator>
          <MaxScaleDenominator>1000001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#a87000</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Bundið slitlag 500þ</Name>
            <Title>Bundið slitlag 500þ</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:And>
                <ogc:PropertyIsGreaterThanOrEqualTo>
                  <ogc:PropertyName>kortbirta</ogc:PropertyName>
                  <ogc:Literal>750000</ogc:Literal>
                </ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                  <ogc:Literal>Bundið slitlag</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>einingvegakerfis</ogc:PropertyName>
                <ogc:Literal>1</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100001</MinScaleDenominator>
          <MaxScaleDenominator>500001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#e60000</CssParameter>
              <CssParameter name="stroke-width">0.7</CssParameter>
              <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
		 <Rule>
          <Name>Bundið slitlag göng 500þ</Name>
            <Title>Bundið slitlag göng 500þ</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:And>
                <ogc:PropertyIsGreaterThanOrEqualTo>
                  <ogc:PropertyName>kortbirta</ogc:PropertyName>
                  <ogc:Literal>750000</ogc:Literal>
                </ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                  <ogc:Literal>Bundið slitlag</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>einingvegakerfis</ogc:PropertyName>
                <ogc:Literal>4</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100001</MinScaleDenominator>
          <MaxScaleDenominator>500001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#e60000</CssParameter>
              <CssParameter name="stroke-width">0.7</CssParameter>
              <CssParameter name="stroke-opacity">0.70</CssParameter>
			  <CssParameter name="stroke-dasharray">6 4</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Malarvegur 500þ</Name>
            <Title>Malarvegur 500þ</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kortbirta</ogc:PropertyName>
                <ogc:Literal>750000</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                <ogc:Literal>Malarvegur</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100001</MinScaleDenominator>
          <MaxScaleDenominator>500001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#a87000</CssParameter>
              <CssParameter name="stroke-width">0.7</CssParameter>
              <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Torfarinn slóði 500þ</Name>
            <Title>Torfarinn slóði 500þ</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kortbirta</ogc:PropertyName>
                <ogc:Literal>750000</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                <ogc:Literal>Torfarinn slóði</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100001</MinScaleDenominator>
          <MaxScaleDenominator>500001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#FFFF00</CssParameter>
              <CssParameter name="stroke-opacity">0.70</CssParameter>
              <CssParameter name="stroke-width">1.3</CssParameter>
              <CssParameter name="stroke-dasharray">4 2</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Slóði 500þ</Name>
            <Title>Slóði 500þ</Title>
           <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kortbirta</ogc:PropertyName>
                <ogc:Literal>750000</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                <ogc:Literal>Slóði</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100001</MinScaleDenominator>
          <MaxScaleDenominator>500001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#ffff00</CssParameter>
              <CssParameter name="stroke-width">1.3</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Bundið slitlag 100þ</Name>
            <Title>Bundið slitlag 100þ</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:And>
                <ogc:PropertyIsGreaterThanOrEqualTo>
                  <ogc:PropertyName>kortbirta</ogc:PropertyName>
                  <ogc:Literal>50000</ogc:Literal>
                </ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                  <ogc:Literal>Bundið slitlag</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>einingvegakerfis</ogc:PropertyName>
                <ogc:Literal>1</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100</MinScaleDenominator>
          <MaxScaleDenominator>100001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#e60000</CssParameter>
              <CssParameter name="stroke-width">0.9</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
		<Rule>
          <Name>Bundið slitlag göng 100þ</Name>
            <Title>Bundið slitlag göng 100þ</Title>
        <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:And>
                <ogc:PropertyIsGreaterThanOrEqualTo>
                  <ogc:PropertyName>kortbirta</ogc:PropertyName>
                  <ogc:Literal>50000</ogc:Literal>
                </ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                  <ogc:Literal>Bundið slitlag</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>einingvegakerfis</ogc:PropertyName>
                <ogc:Literal>4</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100</MinScaleDenominator>
          <MaxScaleDenominator>100001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#e60000</CssParameter>
              <CssParameter name="stroke-width">0.9</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
			  <CssParameter name="stroke-dasharray">6 4</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Malarvegur 100þ</Name>
            <Title>Malarvegur 100þ</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kortbirta</ogc:PropertyName>
                <ogc:Literal>50000</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                <ogc:Literal>Malarvegur</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100</MinScaleDenominator>
          <MaxScaleDenominator>100001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#a87000</CssParameter>
              <CssParameter name="stroke-width">0.9</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Slóði 100þ</Name>
            <Title>Slóði 100þ</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kortbirta</ogc:PropertyName>
                <ogc:Literal>50000</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                <ogc:Literal>Slóði</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100</MinScaleDenominator>
          <MaxScaleDenominator>100001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#ffff00</CssParameter>
              <CssParameter name="stroke-width">1.4</CssParameter>
			  <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Torfarinn slóði 100Þ</Name>
            <Title>Torfarinn slóði 100Þ</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kortbirta</ogc:PropertyName>
                <ogc:Literal>50000</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                <ogc:Literal>Torfarinn slóði</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100</MinScaleDenominator>
          <MaxScaleDenominator>100001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#ffff00</CssParameter>
              <CssParameter name="stroke-opacity">0.70</CssParameter>
              <CssParameter name="stroke-width">1.4</CssParameter>
              <CssParameter name="stroke-dasharray">4 2</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <Name>Innanbæjar 100þ</Name>
            <Title>Innanbæjar 100þ</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kortbirta</ogc:PropertyName>
                <ogc:Literal>50000</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kortvegtegund</ogc:PropertyName>
                <ogc:Literal>Innanbæjar</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MinScaleDenominator>100</MinScaleDenominator>
          <MaxScaleDenominator>100001</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#7f7f7f</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
              <CssParameter name="stroke-opacity">0.70</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
		
		<!-- vegnr -->
        <Rule>
          <Name>Vegnúmer stærri vegir</Name>
            <Title>Vegnúmer stærri vegir</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>kortlabel</ogc:PropertyName>
              <ogc:Literal>Aðalvegur</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MinScaleDenominator>1</MinScaleDenominator>
          <MaxScaleDenominator>100000</MaxScaleDenominator>
		  <TextSymbolizer>
                        <Label>
                            <ogc:Function name="strToUpperCase">
                                <ogc:PropertyName>vegnr</ogc:PropertyName>
                            </ogc:Function>
                        </Label>
                        <Font>
                            <CssParameter  name="font-family">Arial</CssParameter >
                            <CssParameter  name="font-size">9.00</CssParameter >
                            <CssParameter  name="font-weight">bold</CssParameter >
                        </Font>
						<LabelPlacement>
                            <LinePlacement />
                        </LabelPlacement>
						<Fill>
							<CssParameter  name="fill">#1E2832 </CssParameter >
						</Fill>
						<Graphic>
                                    <Mark>
                                        <WellKnownName>square</WellKnownName>
										   <Fill>
											 <CssParameter  name="fill"> #fefefe</CssParameter >
										   </Fill>
                                    </Mark>
                        </Graphic>
						<VendorOption name="graphic-resize">stretch</VendorOption>                         
						<VendorOption name="graphic-margin">1</VendorOption>
						<VendorOption name="group">yes</VendorOption>
                    </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Vegnúmer minni vegir</Name>
            <Title>Vegnúmer minni vegir</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>kortlabel</ogc:PropertyName>
              <ogc:Literal>Minni vegir</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MinScaleDenominator>1</MinScaleDenominator>
          <MaxScaleDenominator>40000</MaxScaleDenominator>
		  <TextSymbolizer>
                        <Label>
                            <ogc:Function name="strToUpperCase">
                                <ogc:PropertyName>vegnr</ogc:PropertyName>
                            </ogc:Function>
                        </Label>
                        <Font>
                            <CssParameter  name="font-family">Arial</CssParameter >
                            <CssParameter  name="font-size">9.00</CssParameter >
                            <CssParameter  name="font-weight">bold</CssParameter >
                        </Font>
						<LabelPlacement>
                            <LinePlacement />
                        </LabelPlacement>
						<Fill>
							<CssParameter  name="fill">#1E2832 </CssParameter >
						</Fill>
						<Graphic>
                                    <Mark>
                                        <WellKnownName>square</WellKnownName>
										   <Fill>
											 <CssParameter  name="fill"> #fefefe</CssParameter >
										   </Fill>
                                    </Mark>
                        </Graphic>
						<VendorOption name="graphic-resize">stretch</VendorOption>                         
						<VendorOption name="graphic-margin">1</VendorOption>
						<VendorOption name="group">yes</VendorOption>
                    </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>