<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <!-- a named layer is the basic building block of an sld document -->
    <NamedLayer>
        <Name>asdi:AirfldC</Name>
        <UserStyle>
            <Name>asdi:Airport</Name>
            <Title>Airport</Title>
            <Abstract>A default style for airport</Abstract>
            <FeatureTypeStyle>
                <Rule>
                  <MaxScaleDenominator>500001</MaxScaleDenominator>
                    <PointSymbolizer>
                        <Graphic>
                            <ExternalGraphic>
                                <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="airport.png" />
                                <Format>image/png</Format>
                            </ExternalGraphic>
                            <Mark />
                            <Opacity>0.65</Opacity>
                            <Size>20</Size>
                            <!--Rotation>90</Rotation-->
                        </Graphic>
                    </PointSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
