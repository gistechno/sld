<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"
  xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <NamedLayer>
    <Name></Name>
    <UserStyle>
      <Title>Náttúrulegt birkilendi</Title>
      <FeatureTypeStyle>
        <Rule>
          <Title>Birkikjarr</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
          <ogc:Or>
              <ogc:Or>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>grodurfelag_text</ogc:PropertyName>
              <ogc:Literal>Birkikjarr</ogc:Literal>
            </ogc:PropertyIsEqualTo>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>grodurfelag_text</ogc:PropertyName>
              <ogc:Literal>Birki- og gulvíðiskjarr</ogc:Literal>
            </ogc:PropertyIsEqualTo>
            </ogc:Or>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>grodurfelag_text</ogc:PropertyName>
              <ogc:Literal>Gróðurhverfi án birkis</ogc:Literal>
            </ogc:PropertyIsEqualTo>
            </ogc:Or>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#f6fa74
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#f6fa74</CssParameter>
              <CssParameter name="stroke-opacity">0.50</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Title>Birkiskógur</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>grodurfelag_text</ogc:PropertyName>
              <ogc:Literal>Birkiskógur</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#b7f978
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#b7f978</CssParameter>
              <CssParameter name="stroke-opacity">0.50</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
		    <Rule>
          <Title>Gróðurhverfi án birkis</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>grodurfelag_text</ogc:PropertyName>
              <ogc:Literal>Gróðurhverfi án birkis</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#fda60a
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#fda60a</CssParameter>
              <CssParameter name="stroke-opacity">0.50</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>