<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor version="1.0.0" xmlns:sld="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld ./Sld/StyledLayerDescriptor.xsd">
    <sld:NamedLayer>
        <sld:Name>corine_yfirbord</sld:Name>
        <sld:UserStyle>
            <sld:Name>corine_yfirbord</sld:Name>
            <sld:FeatureTypeStyle>
                <sld:Rule>
                    <sld:Name>211, 231, 321</sld:Name>
                        <sld:Title>211, 231, 321</sld:Title>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:Or>
                            <ogc:Or>
                                <ogc:PropertyIsEqualTo>
                                    <ogc:PropertyName>code_id</ogc:PropertyName>
                                    <ogc:Literal>211</ogc:Literal>
                                </ogc:PropertyIsEqualTo>
                                <ogc:PropertyIsEqualTo>
                                    <ogc:PropertyName>code_id</ogc:PropertyName>
                                    <ogc:Literal>231</ogc:Literal>
                                </ogc:PropertyIsEqualTo>
                            </ogc:Or>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>code_id</ogc:PropertyName>
                                <ogc:Literal>321</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:Or>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>100</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>250001</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#38a800</sld:CssParameter>
                            <sld:CssParameter name="fill-opacity">0.12</sld:CssParameter>
                        </sld:Fill>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>311, 312, 313, 324</sld:Name>
                    <sld:Title>311, 312, 313, 324</sld:Title>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:Or>
                            <ogc:Or>
                                <ogc:Or>
                                    <ogc:PropertyIsEqualTo>
                                        <ogc:PropertyName>code_id</ogc:PropertyName>
                                        <ogc:Literal>311</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                    <ogc:PropertyIsEqualTo>
                                        <ogc:PropertyName>code_id</ogc:PropertyName>
                                        <ogc:Literal>312</ogc:Literal>
                                    </ogc:PropertyIsEqualTo>
                                </ogc:Or>
                                <ogc:PropertyIsEqualTo>
                                    <ogc:PropertyName>code_id</ogc:PropertyName>
                                    <ogc:Literal>313</ogc:Literal>
                                </ogc:PropertyIsEqualTo>
                            </ogc:Or>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>code_id</ogc:PropertyName>
                                <ogc:Literal>324</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:Or>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>100</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>100000</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:GraphicFill>
                                <sld:Graphic>
                                    <sld:ExternalGraphic>
                                        <sld:OnlineResource xlink:type="simple" xlink:href="scrub_graent.png" />
                                        <sld:Format>image/png</sld:Format>
                                    </sld:ExternalGraphic>
                                    <sld:Size>50</sld:Size>
                                </sld:Graphic>
                            </sld:GraphicFill>
							<sld:CssParameter name="fill-opacity">0.30</sld:CssParameter>
                        </sld:Fill>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>331</sld:Name>
                    <sld:Title>331</sld:Title>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>code_id</ogc:PropertyName>
                            <ogc:Literal>331</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>100</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>1000000</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#cccccc</sld:CssParameter>
                            <sld:CssParameter name="fill-opacity">0.34</sld:CssParameter>
                        </sld:Fill>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name>332</sld:Name>
                    <sld:Title>332</sld:Title>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>code_id</ogc:PropertyName>
                            <ogc:Literal>332</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>100</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>100001</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:GraphicFill>
                                <sld:Graphic>
                                    <sld:ExternalGraphic>
                                        <sld:OnlineResource xlink:type="simple" xlink:href="scrub_svart.png" />
                                        <sld:Format>image/png</sld:Format>
                                    </sld:ExternalGraphic>
                                    <sld:Size>50</sld:Size>
                                </sld:Graphic>
                            </sld:GraphicFill>
							<sld:CssParameter name="fill-opacity">0.30</sld:CssParameter>
                        </sld:Fill>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name></sld:Name>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>code_id</ogc:PropertyName>
                            <ogc:Literal>412</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>100</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>100001</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#38a800</sld:CssParameter>
                            <sld:CssParameter name="fill-opacity">0.12</sld:CssParameter>
                        </sld:Fill>
                    </sld:PolygonSymbolizer>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:GraphicFill>
                                <sld:Graphic>
                                    <sld:ExternalGraphic>
                                        <sld:OnlineResource xlink:type="simple" xlink:href="scrub_blatt.png" />
                                        <sld:Format>image/png</sld:Format>
                                    </sld:ExternalGraphic>
                                    <sld:Size>50</sld:Size>
                                </sld:Graphic>
                            </sld:GraphicFill>
                          <sld:CssParameter name="fill-opacity">0.23</sld:CssParameter>
                        </sld:Fill>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
                <sld:Rule>
                    <sld:Name></sld:Name>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>code_id</ogc:PropertyName>
                            <ogc:Literal>423</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <sld:MinScaleDenominator>100</sld:MinScaleDenominator>
                    <sld:MaxScaleDenominator>100000</sld:MaxScaleDenominator>
                    <sld:PolygonSymbolizer>
                        <sld:Fill>
                            <sld:CssParameter name="fill">#cccccc</sld:CssParameter>
                            <sld:CssParameter name="fill-opacity">0.23</sld:CssParameter>
                        </sld:Fill>
                    </sld:PolygonSymbolizer>
                </sld:Rule>
            </sld:FeatureTypeStyle>
        </sld:UserStyle>
    </sld:NamedLayer>
</sld:StyledLayerDescriptor>