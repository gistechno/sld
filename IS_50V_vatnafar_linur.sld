<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>IS 50V vatnafar línur</Name>
        <UserStyle>
            <Title>IS 50V vatnafar línur</Title>
            <FeatureTypeStyle>
                <Rule>
                    <Name>IS 50V vatnafar línur</Name>
                    <Title>IS 50V vatnafar línur</Title>
                    <PolygonSymbolizer>
                         <Stroke>
                            <CssParameter name="stroke">#1f78b4</CssParameter>
                            <CssParameter name="stroke-width">0.2</CssParameter>
                        </Stroke>
                    </PolygonSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>