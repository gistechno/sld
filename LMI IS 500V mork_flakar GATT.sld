<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>is500v_mork_flakar</Name>
        <UserStyle>
            <Title>is500v_mork_flakar</Title>
            <FeatureTypeStyle>
                <Rule>
                    <Name>Þéttbýli</Name>
                    <Title>Þéttbýli</Title>
                    <MinScaleDenominator>1000</MinScaleDenominator>
                    <MaxScaleDenominator>5000</MaxScaleDenominator>
                    <PolygonSymbolizer>
                        <Fill>
                            <CssParameter name="fill">#ffbfbf</CssParameter>
                            <CssParameter name="fill-opacity">1</CssParameter>
                        </Fill>
                        <Stroke>
                            <CssParameter name="stroke">#ffbfbf</CssParameter>
                            <CssParameter name="stroke-width">0.2</CssParameter>
                        </Stroke>
                    </PolygonSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
