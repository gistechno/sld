<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <!-- a named layer is the basic building block of an sld document -->

  <NamedLayer>
    <Name>Vatnajokulsthjodgardur_raudar_punktalinur</Name>
    <UserStyle>
     <Title>Vatnajökulsþjóðgarður, rauðar punktalínur</Title>
      <Abstract>Stíll fyrir mörk</Abstract>
     <FeatureTypeStyle>
		<Rule>
          <Name>Rule 1</Name>
          <Title>Rauðar punktalínur</Title>
         <PolygonSymbolizer>
          <Stroke>
              <CssParameter name="stroke">#FF1616</CssParameter>
              <CssParameter name="stroke-linejoin">bevel</CssParameter>
              <CssParameter name="stroke-dasharray">4 2</CssParameter>
              <CssParameter name="stroke-width">0.8</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>

        </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>