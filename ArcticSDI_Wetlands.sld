<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
  xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
  xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<NamedLayer>
    <Name>Wetland</Name>
    <UserStyle>
      <Name>Wetland</Name>
      <Title>Wetland</Title>
      <Abstract>Wetland</Abstract>
      <FeatureTypeStyle>
      <Rule>
	  <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>code_id</ogc:PropertyName>
              <ogc:Literal>412</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
           <MaxScaleDenominator>500001</MaxScaleDenominator>
          <PolygonSymbolizer>
           <Fill>
            <GraphicFill>
              <Graphic>
                <Mark> 
                  <WellKnownName>shape://horline</WellKnownName>
                  <Stroke>
                    <CssParameter name="stroke">#66CCFF</CssParameter>     
                    <CssParameter name="stroke-width">1</CssParameter>
                   </Stroke>
                </Mark> 
                <Size>4</Size>
               <!--Rotation>90</Rotation-->
              </Graphic>
             </GraphicFill>
           </Fill>
        </PolygonSymbolizer>
      </Rule>
     </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
