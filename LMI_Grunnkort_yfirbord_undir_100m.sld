<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>Yfirborð undir 100m</Name>
        <UserStyle>
            <Title>Yfirborð undir 100m</Title>
            <FeatureTypeStyle>
                <Rule>
                    <Name>Yfirborð undir 100m</Name>
                    <Title>Yfirborð undir 100m</Title>
                    <MinScaleDenominator>100</MinScaleDenominator>
                    <MaxScaleDenominator>8100000</MaxScaleDenominator>
                    <PolygonSymbolizer>
                        <Fill>
                            <CssParameter name="fill">#CCCCCC</CssParameter>
                            <CssParameter name="fill-opacity">0.56</CssParameter>
                        </Fill>
                    </PolygonSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
