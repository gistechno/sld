<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>ornefni_inspire</se:Name>
        <UserStyle>
            <se:Name>ornefni_inspire</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>2000000</se:Name>
                    <se:Description>
                        <se:Title>2000000</se:Title>
                    </se:Description>
					<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>integer</ogc:PropertyName>
                                <ogc:Literal>2000</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
					<se:MaxScaleDenominator>2000001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>text</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-style">normal</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">400</se:VendorOption>
                        <!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
                        <se:VendorOption name="partials">true</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
				<se:Rule>
                    <se:Name>1000000</se:Name>
                    <se:Description>
                        <se:Title>1000000</se:Title>
                    </se:Description>
					<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>integer</ogc:PropertyName>
                                <ogc:Literal>1000</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
					<se:MaxScaleDenominator>1000001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>text</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-style">normal</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">400</se:VendorOption>
                        <!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
                        <se:VendorOption name="partials">true</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
				<se:Rule>
                    <se:Name>500000</se:Name>
                    <se:Description>
                        <se:Title>500000</se:Title>
                    </se:Description>
					<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>integer</ogc:PropertyName>
                                <ogc:Literal>500</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
					<se:MaxScaleDenominator>500001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>text</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-style">normal</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">400</se:VendorOption>
                        <!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
                        <se:VendorOption name="partials">true</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
				<se:Rule>
                    <se:Name>250000</se:Name>
                    <se:Description>
                        <se:Title>250000</se:Title>
                    </se:Description>
					<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>integer</ogc:PropertyName>
                                <ogc:Literal>250</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
					<se:MaxScaleDenominator>250001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>text</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-style">normal</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">400</se:VendorOption>
                        <!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
                        <se:VendorOption name="partials">true</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
				<se:Rule>
                    <se:Name>100000</se:Name>
                    <se:Description>
                        <se:Title>100000</se:Title>
                    </se:Description>
					<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
							<ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>integer</ogc:PropertyName>
                                <ogc:Literal>100</ogc:Literal>
							</ogc:PropertyIsEqualTo>
                    </ogc:Filter>
					<se:MaxScaleDenominator>100001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>text</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-style">normal</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">400</se:VendorOption>
                        <!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
                        <se:VendorOption name="partials">true</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
				<se:Rule>
                    <se:Name>S0000</se:Name>
                    <se:Description>
                        <se:Title>50000</se:Title>
                    </se:Description>
					<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>integer</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
					<se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>text</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-style">normal</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">400</se:VendorOption>
                        <!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
                        <se:VendorOption name="partials">true</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
				<se:Rule>
                    <se:Name>25000</se:Name>
                    <se:Description>
                        <se:Title>25000</se:Title>
                    </se:Description>
					<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>integer</ogc:PropertyName>
                                <ogc:Literal>25</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
					<se:MaxScaleDenominator>25001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>text</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-style">normal</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">400</se:VendorOption>
                        <!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
                        <se:VendorOption name="partials">true</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
				<se:Rule>
                    <se:Name>10000</se:Name>
                    <se:Description>
                        <se:Title>10000</se:Title>
                    </se:Description>
					<ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>integer</ogc:PropertyName>
                                <ogc:Literal>10</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
					<se:MaxScaleDenominator>10001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>text</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-style">normal</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">400</se:VendorOption>
                        <!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
                        <se:VendorOption name="partials">true</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
 </StyledLayerDescriptor>