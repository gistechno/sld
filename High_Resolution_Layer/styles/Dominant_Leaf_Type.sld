<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.0.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" xmlns:sld="http://www.opengis.net/sld">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>Dominant_Leaf_Type</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
           <sld:ColorMap type="values" extended="true">
             <sld:ColorMapEntry color="#f0f0f0" quantity="0" label="0"/>
             <sld:ColorMapEntry color="#469e4a" quantity="1" label="1"/>
             <sld:ColorMapEntry color="#1c5c24" quantity="2" label="2"/>
            <sld:ColorMapEntry color="#999999" quantity="254" label="254"/>
        	</sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>