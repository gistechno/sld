<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>grassland</Name>
    <UserStyle>
      <Title>grassland</Title>
     <FeatureTypeStyle>
     <Rule>
       <RasterSymbolizer>
         <Opacity>1</Opacity>
         <ColorMap type="values">  
           <ColorMapEntry color="#f0f0f0" quantity="0" opacity="0.6"/>
           <ColorMapEntry color="#a3c738" quantity="1" opacity="0.6"/>
           <ColorMapEntry color="#999999" quantity="254" opacity="0.6"/>
           <ColorMapEntry color="#000000" quantity="255" opacity="0.6"/>
           </ColorMap>
       </RasterSymbolizer>
     </Rule>
   </FeatureTypeStyle>
  </UserStyle>
</NamedLayer>
</StyledLayerDescriptor>