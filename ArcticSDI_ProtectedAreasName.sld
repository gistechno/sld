<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>fridlystsvaedi_lable</Name>
        <UserStyle>
            <Name>fridlystsvaedi_lable</Name>
            <FeatureTypeStyle>
                <Rule>
                    <Name>only names</Name>
                        <Title>only names</Title>
                    <!--MaxScaleDenominator>100001</MaxScaleDenominator-->
                    <TextSymbolizer>
                        <Label>
                            <ogc:PropertyName>nafnFitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">10</CssParameter>
                            <!--CssParameter name="font-color">#4C7300</CssParameter-->
                        </Font>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<Fill>
							<CssParameter name="fill">#4C7300</CssParameter>
						</Fill>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
                        <VendorOption name="polygonAlign">ortho</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
                    </TextSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>