<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>AdministrativeBoundary</Name>
    <UserStyle>
     <Title>AdministrativeBoundary</Title>
      <Abstract>INSPIRE AdministrativeBoundary</Abstract>
     <FeatureTypeStyle>
        <Rule>
          <Name>single_symbol</Name>
          <LineSymbolizer>
			<Stroke>
              <CssParameter name="stroke">#FF0033</CssParameter>
              <CssParameter name="stroke-width">4.0</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
	  </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>