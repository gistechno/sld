<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>watercourse</se:Name>
    <UserStyle>
      <se:Name>watercourse</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>Natuarl</se:Name>
          <se:Description>
            <se:Title>Natural</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>origin</ogc:PropertyName>
              <ogc:Literal>natural</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:LineSymbolizer>
            <se:Stroke>
              <se:SvgParameter name="stroke">#33CCFF</se:SvgParameter>
              <se:SvgParameter name="stroke-width">1</se:SvgParameter>
			</se:Stroke>
          </se:LineSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>Manmade</se:Name>
          <se:Description>
            <se:Title>Manmade</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>origin</ogc:PropertyName>
              <ogc:Literal>manMade</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		  <se:LineSymbolizer>
		  <se:Stroke>
              <se:SvgParameter name="stroke">#0066FF</se:SvgParameter>
              <se:SvgParameter name="stroke-width">1</se:SvgParameter>
          </se:Stroke>
		 </se:LineSymbolizer>
		 </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>