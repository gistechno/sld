<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <!-- a named layer is the basic building block of an sld document -->

  <NamedLayer>
    <Name>TN.RoadTransportNetwork.RoadLink</Name>
    <UserStyle>
      
      
      <Title>TN.RoadTransportNetwork.RoadLink Default style</Title>
      <Abstract>INSPIRE RoadLink</Abstract>
     

      <FeatureTypeStyle>
       
        <Rule>
          <Name>single_symbol</Name>
		  
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#A008000</CssParameter>
			 <CssParameter name="stroke-width">3</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
	  </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>