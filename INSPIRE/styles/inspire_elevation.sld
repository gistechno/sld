<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 

  <NamedLayer>
    <Name>EL.ContourLine</Name>
    <UserStyle>
     
      
      <Title>EL.ContourLine Default style</Title>
      <Abstract>INSPIRE ContourLine</Abstract>
      

      <FeatureTypeStyle>
       
        <Rule>
          <Name>single_symbol</Name>
		  <MinScaleDenominator>1</MinScaleDenominator>
          <MaxScaleDenominator>1000001</MaxScaleDenominator>
        
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#A47916</CssParameter>
			 <CssParameter name="stroke-width">1</CssParameter>
            </Stroke>
          </LineSymbolizer>
        </Rule>
	  </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>