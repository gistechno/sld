<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">


  <NamedLayer>
    <Name>HY.PhysicalWaters.Wetland</Name>
    <UserStyle>
       <Title>HY.PhysicalWaters.Wetland default style</Title>
      <Abstract>INSPIRE wetland hydro</Abstract>
		<FeatureTypeStyle>
		<Rule>
          <Name>single_symbol</Name>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#00CCCC</CssParameter>
            </Fill>
          </PolygonSymbolizer>
        </Rule>
	  </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>