<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <!-- a named layer is the basic building block of an sld document -->

  <NamedLayer>
    <Name>Miðhálendislína</Name>
    <UserStyle>
     <Title>Rauð miðhálendislína</Title>
      <Abstract>Stíll fyrir miðhálendislína</Abstract>
     <FeatureTypeStyle>
		<Rule>
          <Name>Rule 1</Name>
          <Title>Rauð lína</Title>
         <PolygonSymbolizer>
		   <Fill>
              <CssParameter name="fill">#d364d3</CssParameter>
              <CssParameter name="fill-opacity">0.00</CssParameter>
            </Fill>
          <Stroke>
             <CssParameter name="stroke">#e31a1c</CssParameter>
			<CssParameter name="stroke-width">0.5</CssParameter>
			<CssParameter name="stroke-linejoin">bevel</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
	</FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>