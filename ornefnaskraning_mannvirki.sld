<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>mannvirki_punktar</se:Name>
    <UserStyle>
      <se:Name>mannvirki_punktar</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>Virkt fyrirbæri, í notkun</se:Name>
          <se:Description>
            <se:Title>Virkt fyrirbæri, í notkun</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>virkni</ogc:PropertyName>
              <ogc:Literal>1</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:MinScaleDenominator>1000</se:MinScaleDenominator>
          <se:MaxScaleDenominator>100000</se:MaxScaleDenominator>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#0deb4c</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>5</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
           <se:TextSymbolizer>
         <se:Label>
           <ogc:PropertyName>nafnfitju</ogc:PropertyName>
         </se:Label>
         <se:Font>
          <se:SvgParameter name="font-family">Century Gothic</se:SvgParameter>
          <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
          <se:SvgParameter name="font-size">12</se:SvgParameter>
          </se:Font>
          <se:LabelPlacement>
            <se:LinePlacement />
          </se:LabelPlacement>
         <se:Halo>
            <se:Fill>
              <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
            </se:Fill>
         </se:Halo>
         <se:VendorOption name="polygonAlign">mbr</se:VendorOption>
         </se:TextSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>Óvirkt fyrirbæri, ekki í notkun</se:Name>
          <se:Description>
            <se:Title>Óvirkt fyrirbæri, ekki í notkun</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>virkni</ogc:PropertyName>
              <ogc:Literal>2</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:MinScaleDenominator>1000</se:MinScaleDenominator>
          <se:MaxScaleDenominator>100001</se:MaxScaleDenominator>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#e40627</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>5</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
          <se:TextSymbolizer>
         <se:Label>
           <ogc:PropertyName>nafnfitju</ogc:PropertyName>
         </se:Label>
         <se:Font>
          <se:SvgParameter name="font-family">Century Gothic</se:SvgParameter>
          <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
          <se:SvgParameter name="font-size">12</se:SvgParameter>
          </se:Font>
          <se:LabelPlacement>
            <se:LinePlacement />
          </se:LabelPlacement>
         <se:Halo>
            <se:Fill>
              <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
            </se:Fill>
         </se:Halo>
         <se:VendorOption name="polygonAlign">mbr</se:VendorOption>
         </se:TextSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>