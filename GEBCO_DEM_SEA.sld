<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>landhaedalikan</Name>
    <UserStyle>
      <Title>landhaedalikan</Title>
     <FeatureTypeStyle>
     <Rule>
       <RasterSymbolizer>
         <Opacity>1</Opacity>
         <ColorMap type="intervals">
           <ColorMapEntry color="#29c9ff" quantity="-3260" />
           <ColorMapEntry color="#33ccff" quantity="-3000" />
           <ColorMapEntry color="#3dcfff" quantity="-2500" />
           <ColorMapEntry color="#47d1ff" quantity="-2000" />
           <ColorMapEntry color="#52d4ff" quantity="-1500" />
           <ColorMapEntry color="#5cd6ff" quantity="-1000" />
           <ColorMapEntry color="#66d9ff" quantity="-500" />
           <ColorMapEntry color="#70dbff" quantity="-400" />
           <ColorMapEntry color="#7adeff" quantity="-300" />
           <ColorMapEntry color="#85e0ff" quantity="-200" />
           <ColorMapEntry color="#8fe3ff" quantity="-1" />
           <ColorMapEntry color="#c95c3d" quantity="32767" opacity="0"/>
         </ColorMap>
       </RasterSymbolizer>
     </Rule>
   </FeatureTypeStyle>
  </UserStyle>
</NamedLayer>
</StyledLayerDescriptor>
