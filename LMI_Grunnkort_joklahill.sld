<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>raster_layer</Name>
    <UserStyle>
      <Title>raster_layer</Title>
     <FeatureTypeStyle>
     <Rule>
       <RasterSymbolizer>
         <Opacity>0.6</Opacity>
         <ColorMap>
           <ColorMapEntry color="#000000" opacity="0.0" quantity="0.0"/>
           <ColorMapEntry color="#C2E9FF" opacity="1" quantity="1.0"/>
           <ColorMapEntry color="#FFFFFF" opacity="1" quantity="256.0"/>
         </ColorMap>
       </RasterSymbolizer>
     </Rule>
   </FeatureTypeStyle>
  </UserStyle>
</NamedLayer>
</StyledLayerDescriptor>
