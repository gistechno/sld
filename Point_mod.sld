<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
		xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
		xmlns="http://www.opengis.net/sld" 
		xmlns:ogc="http://www.opengis.net/ogc" 
		xmlns:xlink="http://www.w3.org/1999/xlink" 
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<!-- a named layer is the basic building block of an sld document -->

	<NamedLayer>
		<Name>Hrágögn punktar</Name>
		<UserStyle>
		  
			<Title>Hrágögn punktar</Title>
			<Abstract>Rauðir punktar með ljósir miðju</Abstract>

			<FeatureTypeStyle>
				<Rule>
					<Name>Rule 1</Name>
					<Title>Rauður punktur</Title>
					<Abstract>Rauðir punktar með ljósri miðju</Abstract>

					<PointSymbolizer>
						<Graphic>
							<Mark>
								<WellKnownName>circle</WellKnownName>
								<Fill>
									<CssParameter name="fill">#FF0000</CssParameter>
                                	<CssParameter name="fill-opacity">0.2</CssParameter>
								</Fill>
                                <Stroke>
               						<CssParameter name="stroke">#FF0000</CssParameter>
                                  	<CssParameter name="stroke-opacity">0.5</CssParameter>
               						<CssParameter name="stroke-width">0.5</CssParameter>
             					</Stroke>
							</Mark>
							<Size>4</Size>
						</Graphic>
					</PointSymbolizer>
				</Rule>

		    </FeatureTypeStyle>
		</UserStyle>
	</NamedLayer>
</StyledLayerDescriptor>