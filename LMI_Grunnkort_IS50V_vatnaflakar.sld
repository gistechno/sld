<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>IS 50V vatn</Name>
        <UserStyle>
            <Title>IS 50V vatn</Title>
            <FeatureTypeStyle>
                <Rule>
                    <Name>IS 50V vatn</Name>
                    <Title>IS 50V vatn</Title>
                    <ogc:Filter>
                        <ogc:PropertyIsNotEqualTo>
                            <ogc:PropertyName>yfirbordsvatn</ogc:PropertyName>
                            <ogc:Literal>4</ogc:Literal>
                        </ogc:PropertyIsNotEqualTo>
                    </ogc:Filter>
                    <MinScaleDenominator>100</MinScaleDenominator>
                    <MaxScaleDenominator>249900</MaxScaleDenominator>
                    <PolygonSymbolizer>
                        <Fill>
                            <CssParameter name="fill">#B3E6FF</CssParameter>
                        </Fill>
                    </PolygonSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
