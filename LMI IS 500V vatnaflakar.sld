<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>Mannvirki Flákar</Name>
        <UserStyle>
            <Title>Mannvirki flákar</Title>
            <FeatureTypeStyle>
                <Rule>
                    <Name>Þéttbýli</Name>
                    <Title>Þéttbýli</Title>
                    <ogc:Filter>
                        <ogc:PropertyIsNotEqualTo>
                            <ogc:PropertyName>yfirbordsvatn</ogc:PropertyName>
                            <ogc:Literal>4</ogc:Literal>
                        </ogc:PropertyIsNotEqualTo>
                    </ogc:Filter>
                    <MinScaleDenominator>250000</MinScaleDenominator>
                    <MaxScaleDenominator>8000000</MaxScaleDenominator>
                    <PolygonSymbolizer>
                        <Fill>
                            <CssParameter name="fill">#c4ebff</CssParameter>
                        </Fill>
                        <Stroke>
                            <CssParameter name="stroke">#c4ebff</CssParameter>
                            <CssParameter name="stroke-width">0.2</CssParameter>
                        </Stroke>
                    </PolygonSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
