<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"
  xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <NamedLayer>
    <Name>Bændur græða landið</Name>
    <UserStyle>
      <Title>Bændur græða landið</Title>
      <FeatureTypeStyle>
        <Rule>
          <Title>Bændur græða landið</Title>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#fbb4b4
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#fb9e87</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
            </Stroke>
          </PolygonSymbolizer>

        </Rule>

      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>