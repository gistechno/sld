<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>Miðlunarlón úr IS 50V</Name>
        <UserStyle>
            <Title>Miðlunarlón úr IS 50V</Title>
            <FeatureTypeStyle>
                <Rule>
                    <Name>Miðlunarlón úr IS 50V</Name>
                    <Title>Miðlunarlón úr IS 50V</Title>
                    <ogc:Filter>
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>ahrifmanna</ogc:PropertyName>
                            <ogc:Literal>3</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <PolygonSymbolizer>
                        <Fill>
                            <CssParameter name="fill">#2d54f0</CssParameter>
                        </Fill>
                        <Stroke>
                            <CssParameter name="stroke">#2d54f0</CssParameter>
                            <CssParameter name="stroke-width">0.2</CssParameter>
                        </Stroke>
                    </PolygonSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>