<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>IS 50V vatnalínur</Name>
    <UserStyle>
      <Name>IS 50V vatnalínur</Name>
      <FeatureTypeStyle>
        <Rule>
          <Name>Single symbol</Name>
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>yfirbordsvatn</ogc:PropertyName>
              <ogc:Literal>1</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <LineSymbolizer>
            <Stroke>
              <CssParameter  name="stroke">#C4EBFF</CssParameter >
              <CssParameter  name="stroke-width">0.2</CssParameter >
              <CssParameter  name="stroke-linejoin">bevel</CssParameter >
              <CssParameter  name="stroke-linecap">square</CssParameter >
            </Stroke>
          </LineSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
