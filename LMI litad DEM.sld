<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>landhaedalikan</Name>
    <UserStyle>
      <Title>landhaedalikan</Title>
     <FeatureTypeStyle>
     <Rule>
       <RasterSymbolizer>
         <Opacity>1</Opacity>
         <ColorMap type="ramp">
           <ColorMapEntry color="#d1fcc2" quantity="100" />
           <ColorMapEntry color="#ebfab3" quantity="200" />
           <ColorMapEntry color="#f5fab3" quantity="300" />
           <ColorMapEntry color="#ffffb3" quantity="400" />
           <ColorMapEntry color="#fff7b3" quantity="500" />
           <ColorMapEntry color="#fcf0b3" quantity="600" />
           <ColorMapEntry color="#fae6b0" quantity="700" />
           <ColorMapEntry color="#f5dbad" quantity="800" />
           <ColorMapEntry color="#f5d4ad" quantity="900" />
           <ColorMapEntry color="#ebcca8" quantity="1000" />
           <ColorMapEntry color="#e6c2a1" quantity="1100" />
           <ColorMapEntry color="#e0b896" quantity="1200" />
           <ColorMapEntry color="#dead8c" quantity="1300" />
           <ColorMapEntry color="#dba382" quantity="1400" />
           <ColorMapEntry color="#d99978" quantity="1500" />
           <ColorMapEntry color="#d68f6e" quantity="1600" />
           <ColorMapEntry color="#d48563" quantity="1700" />
           <ColorMapEntry color="#d17a59" quantity="1800" />
           <ColorMapEntry color="#cf704f" quantity="1900" />
           <ColorMapEntry color="#cc6645" quantity="2000" />
           <ColorMapEntry color="#c95c3d" quantity="2100" />
         </ColorMap>
       </RasterSymbolizer>
     </Rule>
   </FeatureTypeStyle>
  </UserStyle>
</NamedLayer>
</StyledLayerDescriptor>
