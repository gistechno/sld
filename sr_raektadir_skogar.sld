<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"
  xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <NamedLayer>
    <Name></Name>
    <UserStyle>
      <Title>Ræktað skóglendi</Title>
      <FeatureTypeStyle>
        <Rule>
          <Title>Ræktaðir eldri skógar</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
          <ogc:Or>
              <ogc:Or>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>aldursflokkur_texti</ogc:PropertyName>
              <ogc:Literal>Frekar ungur (15-30 ára)</ogc:Literal>
            </ogc:PropertyIsEqualTo>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>aldursflokkur_texti</ogc:PropertyName>
              <ogc:Literal>Á vaxtarskeiði (30-60 ára)</ogc:Literal>
            </ogc:PropertyIsEqualTo>
            </ogc:Or>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>aldursflokkur_texti</ogc:PropertyName>
              <ogc:Literal>Fullvaxta (60-100 ára)</ogc:Literal>
            </ogc:PropertyIsEqualTo>
			<ogc:PropertyIsEqualTo>
              <ogc:PropertyName>aldursflokkur_texti</ogc:PropertyName>
              <ogc:Literal>Gamall (>100 ára)</ogc:Literal>
            </ogc:PropertyIsEqualTo>
            </ogc:Or>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#bc7a74
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#bc7a74</CssParameter>
              <CssParameter name="stroke-opacity">0.50</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Title>Ræktaðir yngri skógar</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>aldursflokkur_texti</ogc:PropertyName>
              <ogc:Literal>Ungur (&lt;15 ára)</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#f67a74
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#f67a74</CssParameter>
              <CssParameter name="stroke-opacity">0.50</CssParameter>
              <CssParameter name="stroke-width">0.5</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>