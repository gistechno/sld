<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>NamedLocation_gnamel</se:Name>
        <UserStyle>
            <se:Name>NamedLocation_gnamel</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>NamedLocation_gnamel</se:Name>
                    <se:Description>
                        <se:Title>NamedLocation_gnamel</se:Title>
                    </se:Description>
				   <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>namn1</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-style">normal</se:SvgParameter>
                            <se:SvgParameter name="font-size">6</se:SvgParameter>
                        </se:Font>
						<se:LabelPlacement>
                            <se:LinePlacement />
                        </se:LabelPlacement>
                       <se:Fill>
                            <se:SvgParameter name="fill">#000000</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="maxDisplacement">50</se:VendorOption>
                        <!--se:VendorOption name="conflictResolution">false</se:VendorOption-->
                        <se:VendorOption name="partials">true</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
			</se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
	</StyledLayerDescriptor>