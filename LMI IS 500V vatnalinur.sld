<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>IS 500V vatnalínur</Name>
    <UserStyle>
      <Name>IS 500V vatnalínur</Name>
      <FeatureTypeStyle>
        <Rule>
          <Name>Single symbol</Name>
            <MinScaleDenominator>250001</MinScaleDenominator>
            <MaxScaleDenominator>1000000</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter  name="stroke">#C4EBFF</CssParameter >
              <CssParameter  name="stroke-width">0.2</CssParameter >
              <CssParameter  name="stroke-linejoin">bevel</CssParameter >
              <CssParameter  name="stroke-linecap">square</CssParameter >
            </Stroke>
          </LineSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
