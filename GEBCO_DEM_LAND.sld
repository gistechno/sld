<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>landhaedalikan</Name>
    <UserStyle>
      <Title>landhaedalikan</Title>
     <FeatureTypeStyle>
     <Rule>
       <RasterSymbolizer>
         <Opacity>1</Opacity>
         <ColorMap type="intervals">
           <ColorMapEntry color="#c95c3d" quantity="-1" opacity="0"/>
           <ColorMapEntry color="#d1fcc2" quantity="0" opacity="0.6"/>
           <ColorMapEntry color="#EBFACC" quantity="100" opacity="0.6"/>
           <ColorMapEntry color="#F5FACC" quantity="200" opacity="0.6"/>
           <ColorMapEntry color="#F5FAB3" quantity="300" opacity="0.6"/>
           <ColorMapEntry color="#FFFFB3" quantity="400" opacity="0.6"/>
           <ColorMapEntry color="#FFF7B3" quantity="500" opacity="0.6"/>
           <ColorMapEntry color="#fcf0b3" quantity="600" opacity="0.6"/>
           <ColorMapEntry color="#fae6b0" quantity="700" opacity="0.6"/>
           <ColorMapEntry color="#f5dbad" quantity="800" opacity="0.6"/>
           <ColorMapEntry color="#f5d4ad" quantity="900" opacity="0.6"/>
           <ColorMapEntry color="#ebcca8" quantity="1000" opacity="0.6"/>
           <ColorMapEntry color="#e6c2a1" quantity="1100" opacity="0.6"/>
           <ColorMapEntry color="#e0b896" quantity="1200" opacity="0.6"/>
           <ColorMapEntry color="#dead8c" quantity="1300" opacity="0.6"/>
           <ColorMapEntry color="#dba382" quantity="1400" opacity="0.6"/>
           <ColorMapEntry color="#d99978" quantity="1500" opacity="0.6"/>
           <ColorMapEntry color="#d68f6e" quantity="1600" opacity="0.6"/>
           <ColorMapEntry color="#d48563" quantity="1700" opacity="0.6"/>
           <ColorMapEntry color="#d17a59" quantity="1800" opacity="0.6"/>
           <ColorMapEntry color="#cf704f" quantity="1900" opacity="0.6"/>
           <ColorMapEntry color="#cc6645" quantity="2000" opacity="0.6"/>
           <ColorMapEntry color="#c95c3d" quantity="2100" opacity="0.6"/>
           <ColorMapEntry color="#c95c3d" quantity="32767" opacity="0"/>
         </ColorMap>
       </RasterSymbolizer>
     </Rule>
   </FeatureTypeStyle>
  </UserStyle>
</NamedLayer>
</StyledLayerDescriptor>
