<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0"
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
    xmlns="http://www.opengis.net/sld"
    xmlns:ogc="http://www.opengis.net/ogc"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>ornefni_flakar</Name>
    <UserStyle>
      <Name>ornefni_flakar</Name>
      <FeatureTypeStyle>
        <Rule>
          <Name>Kvardi 2000 Hydro</Name>
            <Title>Kvardi 2000 Hydro</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>2000</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:Or>
                      <ogc:Or>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>6.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>7.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                      </ogc:Or>
                      <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                        <ogc:PropertyName>nafnberi</ogc:PropertyName>
                        <ogc:Literal>8.%</ogc:Literal>
                      </ogc:PropertyIsLike>
                    </ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>9.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>10.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>11.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>500001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#005ceb</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<Fill>
							<CssParameter name="fill">#005CEB</CssParameter>
                        </Fill>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Kvardi 2000 Land</Name>
            <Title>Kvardi 2000 Land</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>2000</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>2.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>3.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>4.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>5.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>500001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#464646</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Kvardi 1000 Hydro</Name>
            <Title>Kvardi 1000 Hydro</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>1000</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:Or>
                      <ogc:Or>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>6.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>7.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                      </ogc:Or>
                      <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                        <ogc:PropertyName>nafnberi</ogc:PropertyName>
                        <ogc:Literal>8.%</ogc:Literal>
                      </ogc:PropertyIsLike>
                    </ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>9.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>10.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>11.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>500001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#005ceb</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<Fill>
							<CssParameter name="fill">#005CEB</CssParameter>
                        </Fill>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Kvardi 1000 Land</Name>
            <Title>Kvardi 1000 Land</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>1000</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>2.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>3.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>4.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>5.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>500001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#464646</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Kvardi 500 Hydro</Name>
            <Title>Kvardi 500 Hydro</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>500</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:Or>
                      <ogc:Or>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>6.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>7.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                      </ogc:Or>
                      <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                        <ogc:PropertyName>nafnberi</ogc:PropertyName>
                        <ogc:Literal>8.%</ogc:Literal>
                      </ogc:PropertyIsLike>
                    </ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>9.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>10.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>11.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>250001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#005ceb</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<Fill>
							<CssParameter name="fill">#005CEB</CssParameter>
                        </Fill>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Kvardi 500 Land</Name>
            <Title>Kvardi 500 Land</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>500</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>2.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>3.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>4.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>5.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>250001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#464646</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Kvardi 250 Hydro</Name>
            <Title>Kvardi 250 Hydro</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>250</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:Or>
                      <ogc:Or>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>6.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>7.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                      </ogc:Or>
                      <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                        <ogc:PropertyName>nafnberi</ogc:PropertyName>
                        <ogc:Literal>8.%</ogc:Literal>
                      </ogc:PropertyIsLike>
                    </ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>9.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>10.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>11.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>100001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#005ceb</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<Fill>
							<CssParameter name="fill">#005CEB</CssParameter>
                        </Fill>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Kvardi 250 Land</Name>
            <Title>Kvardi 250 Land</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>250</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>2.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>3.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>4.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>5.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>100001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#464646</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Kvardi 100 Hydro</Name>
            <Title>Kvardi 100 Hydro</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>100</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:Or>
                      <ogc:Or>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>6.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                        <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                          <ogc:PropertyName>nafnberi</ogc:PropertyName>
                          <ogc:Literal>7.%</ogc:Literal>
                        </ogc:PropertyIsLike>
                      </ogc:Or>
                      <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                        <ogc:PropertyName>nafnberi</ogc:PropertyName>
                        <ogc:Literal>8.%</ogc:Literal>
                      </ogc:PropertyIsLike>
                    </ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>9.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>10.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>11.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>50001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#005ceb</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<Fill>
							<CssParameter name="fill">#005CEB</CssParameter>
                        </Fill>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
        <Rule>
          <Name>Kvardi 100 Land</Name>
            <Title>Kvardi 100 Land</Title>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>100</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:Or>
                <ogc:Or>
                  <ogc:Or>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>2.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                    <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>3.%</ogc:Literal>
                    </ogc:PropertyIsLike>
                  </ogc:Or>
                  <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>4.%</ogc:Literal>
                  </ogc:PropertyIsLike>
                </ogc:Or>
                <ogc:PropertyIsLike escape="\" wildCard="%" singleChar="_">
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>5.%</ogc:Literal>
                </ogc:PropertyIsLike>
              </ogc:Or>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>50001</MaxScaleDenominator>
          <TextSymbolizer>
                        <Label>
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </Label>
                        <Font>
                            <CssParameter name="font-family">Verdana</CssParameter>
                            <CssParameter name="font-size">7</CssParameter>
                            <CssParameter name="font-color">#464646</CssParameter>
                        </Font>
						<LabelPlacement>
							<LinePlacement/>
						</LabelPlacement>
                        <Halo>
                            <Fill>
                                <CssParameter name="fill">#FAFAFA</CssParameter>
								<CssParameter name="fill-opacity">0.55</CssParameter>
                            </Fill>
                        </Halo>
						<VendorOption name="followLine">true</VendorOption>
						<VendorOption name="partials">true</VendorOption>
						 <VendorOption name="group">yes</VendorOption>
						<VendorOption name="maxDisplacement">350</VendorOption>
            </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>