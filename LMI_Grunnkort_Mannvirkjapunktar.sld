<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>mannvirki_punktar</se:Name>
    <UserStyle>
      <se:Name>mannvirki_punktar</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>Sérbýlishús</se:Name>
          <se:Description>
            <se:Title>Sérbýlishús, mælikvarði frá 1:100 þús til 1:50 þús</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>mannvirki</ogc:PropertyName>
              <ogc:Literal>1100</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:MinScaleDenominator>50001</se:MinScaleDenominator>
          <se:MaxScaleDenominator>100001</se:MaxScaleDenominator>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>square</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#000000</se:SvgParameter>
                  <se:SvgParameter name="fill-opacity">0.80</se:SvgParameter>
                </se:Fill>
                <se:Stroke/>
              </se:Mark>
              <se:Size>0.8</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>Sérbýlishús</se:Name>
          <se:Description>
            <se:Title>Sérbýlishús, mælikvarði frá 1:50 þús</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>mannvirki</ogc:PropertyName>
              <ogc:Literal>1100</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:MinScaleDenominator>1000</se:MinScaleDenominator>
          <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>square</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#000000</se:SvgParameter>
                  <se:SvgParameter name="fill-opacity">0.80</se:SvgParameter>
                </se:Fill>
                <se:Stroke/>
              </se:Mark>
              <se:Size>2</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>Frístundahús</se:Name>
          <se:Description>
            <se:Title>Frístundahús, mælikvarði frá 1;50 þús</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>mannvirki</ogc:PropertyName>
                <ogc:Literal>1361</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <se:MinScaleDenominator>1000</se:MinScaleDenominator>
          <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>square</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#000000</se:SvgParameter>
                  <se:SvgParameter name="fill-opacity">0.80</se:SvgParameter>
                </se:Fill>
                <se:Stroke/>
              </se:Mark>
              <se:Size>0.5</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		  <se:Rule>
          <se:Name>Skálar</se:Name>
          <se:Description>
            <se:Title>Skálar, mælikvarði frá 1;50 þús</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>mannvirki</ogc:PropertyName>
                <ogc:Literal>1340</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <se:MinScaleDenominator>1000</se:MinScaleDenominator>
          <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>triangle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#0000ff</se:SvgParameter>
                </se:Fill>
                </se:Mark>
              <se:Size>6</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		 <se:Rule>
          <se:Name>Kirkjur og bænarhús</se:Name>
          <se:Description>
            <se:Title>Kirkjur og bænarhús, mælikvarði frá 1:50 þús</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>mannvirki</ogc:PropertyName>
              <ogc:Literal>3500</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:MinScaleDenominator>1000</se:MinScaleDenominator>
          <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="church.svg"/>
                <se:Format>image/svg</se:Format>
              </se:ExternalGraphic>
              <se:Size>12</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>Vitar</se:Name>
          <se:Description>
            <se:Title>Vitar, mælikvarði frá 1:50 þús</se:Title>
          </se:Description>
         <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>mannvirki</ogc:PropertyName>
                  <ogc:Literal>6510</ogc:Literal>
                </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          <se:MinScaleDenominator>1000</se:MinScaleDenominator>
          <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="stardot.png"/>
                <se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>16</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
		<se:Name>Önnur mannvirki</se:Name>
          <se:Description>
            <se:Title>Önnur mannvirki, mælikvarði frá 1:10 þús</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
			<ogc:And>
              <ogc:And>
                <ogc:And>
                  <ogc:And>
                    <ogc:PropertyIsNotEqualTo>
                      <ogc:PropertyName>mannvirki</ogc:PropertyName>
                      <ogc:Literal>1100</ogc:Literal>
                    </ogc:PropertyIsNotEqualTo>
                    <ogc:PropertyIsNotEqualTo>
                      <ogc:PropertyName>mannvirki</ogc:PropertyName>
                      <ogc:Literal>1340</ogc:Literal>
                    </ogc:PropertyIsNotEqualTo>
                  </ogc:And>
                  <ogc:PropertyIsNotEqualTo>
                    <ogc:PropertyName>mannvirki</ogc:PropertyName>
                    <ogc:Literal>1361</ogc:Literal>
                  </ogc:PropertyIsNotEqualTo>
                </ogc:And>
                <ogc:PropertyIsNotEqualTo>
                  <ogc:PropertyName>mannvirki</ogc:PropertyName>
                  <ogc:Literal>6510</ogc:Literal>
                </ogc:PropertyIsNotEqualTo>
              </ogc:And>
              <ogc:PropertyIsNotEqualTo>
                <ogc:PropertyName>mannvirki</ogc:PropertyName>
                <ogc:Literal>3500</ogc:Literal>
              </ogc:PropertyIsNotEqualTo>
            </ogc:And>
          </ogc:Filter>
          <se:MinScaleDenominator>1000</se:MinScaleDenominator>
          <se:MaxScaleDenominator>10001</se:MaxScaleDenominator>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>triangle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#000000</se:SvgParameter>
				  <se:SvgParameter name="fill-opacity">0.8</se:SvgParameter>
                </se:Fill>
				<se:Strole/>
				</se:Mark>
              <se:Size>2</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
	 </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>