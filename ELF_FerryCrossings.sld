<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>FerryCrossings</se:Name>
    <UserStyle>
      <se:Name>FerryCrossings</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name></se:Name>
          <se:MaxScaleDenominator>500001</se:MaxScaleDenominator>
          <se:LineSymbolizer>
            <se:Stroke>
                <se:SvgParameter name="stroke">#7BB3EA</se:SvgParameter>
                <se:SvgParameter name="stroke-width">1.00</se:SvgParameter>
                <se:SvgParameter name="stroke-dasharray">6.00 5.00 6.00 5.00</se:SvgParameter>
            </se:Stroke>
          </se:LineSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>