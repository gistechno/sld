<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>ornefni_flakar</se:Name>
        <UserStyle>
            <se:Name>ornefni_flakar</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>Þéttbýli 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Þéttbýli 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>10</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Century Gothic</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Sveit 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Sveit 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>11</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:Function name="strToUpperCase">
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                            </ogc:Function>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Verdana</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Landörnefni Stór 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Landörnefni Stór 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>21</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:Function name="strToUpperCase">
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                            </ogc:Function>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Landörnefni Mið 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Landörnefni Mið 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>22</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
				<se:Rule>
                    <se:Name>Landörnefni Lítil 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Landörnefni Lítil 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>23</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Sjávarörnefni Stór 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Sjávarörnefni Stór 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>31</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:Function name="strToUpperCase">
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                            </ogc:Function>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">13</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#005CEB</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
				<se:Rule>
                    <se:Name>Sjávarörnefni Mið 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Sjávarörnefni Mið 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>32</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#005CEB</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Sjávarörnefni Lítil 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Sjávarörnefni Lítil 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>33</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#005CEB</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Vatnaörnefni Stór 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Vatnaörnefni Stór 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>41</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:Function name="strToUpperCase">
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                            </ogc:Function>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#005CEB</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Vatnaörnefni Mið 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Vatnaörnefni Mið 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>42</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#005CEB</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Vatnaörnefni Lítil 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Vatnaörnefni Lítil 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>43</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#005CEB</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Jökla- og snævarörnefni Stór 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Jökla- og snævarörnefni Stór 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>51</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:Function name="strToUpperCase">
                                <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                            </ogc:Function>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">13</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#005CEB</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Jökla- og snævarörnefni Mið 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Jökla- og snævarörnefni Mið 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>52</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#005CEB</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>Jökla- og snævarörnefni Lítil 50 til 25</se:Name>
                    <se:Description>
                        <se:Title>Jökla- og snævarörnefni Lítil 50 til 25</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:And>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>kvardi</ogc:PropertyName>
                                <ogc:Literal>50</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                            <ogc:PropertyIsEqualTo>
                                <ogc:PropertyName>ornefnaflokkur</ogc:PropertyName>
                                <ogc:Literal>53</ogc:Literal>
                            </ogc:PropertyIsEqualTo>
                        </ogc:And>
                    </ogc:Filter>
                    <se:MaxScaleDenominator>50001</se:MaxScaleDenominator>
                    <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">Arial</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">11</se:SvgParameter>
							<se:SvgParameter name="font-weight">bold</se:SvgParameter>
                        </se:Font>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
								<se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <se:Fill>
                            <se:SvgParameter name="fill">#005CEB</se:SvgParameter>
                        </se:Fill>
                        <se:VendorOption name="partials">true</se:VendorOption>
						 <se:VendorOption name="group">yes</se:VendorOption>
						 <se:VendorOption name="maxDisplacement ">150</se:VendorOption>
						 <se:VendorOption name="polygonAlign">ortho</se:VendorOption>
                    </se:TextSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
