<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>ornefni_flakar</se:Name>
    <UserStyle>
      <se:Name>ornefni_flakar</se:Name>
      <se:FeatureTypeStyle>    
 <se:Rule>
          <se:Name>250th haf</se:Name>
          <se:Description>
            <se:Title>250th haf</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>250</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>nafnberi</ogc:PropertyName>
                <ogc:Literal>7</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
		  <se:MaxScaleDenominator>72200</se:MaxScaleDenominator>
          <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">verdana</se:SvgParameter>
                            <se:SvgParameter name="font-size">8.00</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#007FFF</se:SvgParameter>
                        </se:Fill>
                    </se:TextSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>250th annad</se:Name>
          <se:Description>
            <se:Title>250th annad</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:And>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>kvardi</ogc:PropertyName>
                <ogc:Literal>250</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
              <ogc:PropertyIsLessThan>
                <ogc:PropertyName>nafnberi</ogc:PropertyName>
                <ogc:Literal>7</ogc:Literal>
              </ogc:PropertyIsLessThan>
            </ogc:And>
          </ogc:Filter>
		  <se:MaxScaleDenominator>72200</se:MaxScaleDenominator>
          <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">verdana</se:SvgParameter>
                            <se:SvgParameter name="font-size">8.00</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#323232</se:SvgParameter>
                        </se:Fill>
                    </se:TextSymbolizer>
        </se:Rule>
	   <se:Rule>
          <se:Name>250th Mannvirki</se:Name>
          <se:Description>
            <se:Title>250th Mannvirki</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:Or>
              <ogc:Or>
                <ogc:Or>
                  <ogc:And>
                    <ogc:PropertyIsEqualTo>
                      <ogc:PropertyName>kvardi</ogc:PropertyName>
                      <ogc:Literal>2000</ogc:Literal>
                    </ogc:PropertyIsEqualTo>
                    <ogc:PropertyIsEqualTo>
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>1.8</ogc:Literal>
                    </ogc:PropertyIsEqualTo>
                  </ogc:And>
                  <ogc:And>
                    <ogc:PropertyIsEqualTo>
                      <ogc:PropertyName>kvardi</ogc:PropertyName>
                      <ogc:Literal>1000</ogc:Literal>
                    </ogc:PropertyIsEqualTo>
                    <ogc:PropertyIsEqualTo>
                      <ogc:PropertyName>nafnberi</ogc:PropertyName>
                      <ogc:Literal>1.8</ogc:Literal>
                    </ogc:PropertyIsEqualTo>
                  </ogc:And>
                </ogc:Or>
                <ogc:And>
                  <ogc:PropertyIsEqualTo>
                    <ogc:PropertyName>kvardi</ogc:PropertyName>
                    <ogc:Literal>500</ogc:Literal>
                  </ogc:PropertyIsEqualTo>
                  <ogc:PropertyIsEqualTo>
                    <ogc:PropertyName>nafnberi</ogc:PropertyName>
                    <ogc:Literal>1.8</ogc:Literal>
                  </ogc:PropertyIsEqualTo>
                </ogc:And>
              </ogc:Or>
              <ogc:And>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>kvardi</ogc:PropertyName>
                  <ogc:Literal>250</ogc:Literal>
                </ogc:PropertyIsEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>nafnberi</ogc:PropertyName>
                  <ogc:Literal>1.8</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:And>
            </ogc:Or>
          </ogc:Filter>
		  <se:MaxScaleDenominator>72200</se:MaxScaleDenominator>
          <se:TextSymbolizer>
                        <se:Label>
                            <ogc:PropertyName>nafnfitju</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">verdana</se:SvgParameter>
                            <se:SvgParameter name="font-size">9.00</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                            <se:PointPlacement>
                                <se:AnchorPoint>
                                    <se:AnchorPointX>0.5</se:AnchorPointX>
                                    <se:AnchorPointY>0.5</se:AnchorPointY>
                                </se:AnchorPoint>
                            </se:PointPlacement>
                        </se:LabelPlacement>
                        <se:Fill>
                            <se:SvgParameter name="fill">#323232</se:SvgParameter>
                        </se:Fill>
                    </se:TextSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>