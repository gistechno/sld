<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <!-- a named layer is the basic building block of an sld document -->

  <NamedLayer>
    <Name>Blár fláki, sjór</Name>
    <UserStyle>
     <Title>Blár fláki, sjór</Title>
      <Abstract>Stíll hafiið</Abstract>
     <FeatureTypeStyle>
		<Rule>
          <Name>Rule 1</Name>
          <Title>Blár fláki</Title>
         <PolygonSymbolizer>
             <Fill>
              <CssParameter name="fill">#30C6FA</CssParameter>
              <CssParameter name="fill-opacity">0.2</CssParameter>
            </Fill>
          <Stroke>
              <CssParameter name="stroke">#30C6FA</CssParameter>
              <CssParameter name="stroke-opscity">0.70</CssParameter>
			 <CssParameter name="stroke-width">0.2</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>

        </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>