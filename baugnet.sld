<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>baugnet</se:Name>
        <UserStyle>
            <se:Name>baugnet</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>grid1</se:Name>
                    <se:Description>
                        <se:Title>grid1</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>level</ogc:PropertyName>
                            <ogc:Literal>0</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <se:MinScaleDenominator>100000</se:MinScaleDenominator>
                    <se:MaxScaleDenominator>5000000</se:MaxScaleDenominator>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#adadad</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">0.7</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-linecap">square</se:SvgParameter>
                            <se:SvgParameter name="stroke-dasharray">4 4</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                    <se:TextSymbolizer>
                        <!--se:Geometry>
                            <ogc:Function name="startPoint">
                                <ogc:PropertyName>geom</ogc:PropertyName>
                            </ogc:Function>
                        </se:Geometry-->
                        <se:Label>
                            <ogc:PropertyName>dmm</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">verdana</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                        <se:LinePlacement>
                            <se:PerpendicularOffset>
                                5
                            </se:PerpendicularOffset>
                        </se:LinePlacement>
                            <!--se:PointPlacement>
                                <se:Rotation>
                                    <ogc:Function name="startAngle">
                                        <ogc:PropertyName>geom</ogc:PropertyName>
                                    </ogc:Function>
                                </se:Rotation>
                            </se:PointPlacement-->
                        </se:LabelPlacement>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
                                <se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <!--se:VendorOption name="partials">true</se:VendorOption-->
                        <!--se:VendorOption name="group">yes</se:VendorOption-->
                        <se:VendorOption name="followLine">true</se:VendorOption>
                        <!--se:VendorOption name="maxDisplacement ">250</se:VendorOption-->
                    </se:TextSymbolizer>
                </se:Rule>
                <se:Rule>
                    <se:Name>grid2</se:Name>
                    <se:Description>
                        <se:Title>grid2</se:Title>
                    </se:Description>
                    <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
                        <ogc:PropertyIsEqualTo>
                            <ogc:PropertyName>level</ogc:PropertyName>
                            <ogc:Literal>1</ogc:Literal>
                        </ogc:PropertyIsEqualTo>
                    </ogc:Filter>
                    <se:MinScaleDenominator>25000</se:MinScaleDenominator>
                    <se:MaxScaleDenominator>249998</se:MaxScaleDenominator>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#adadad</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">0.7</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-linecap">square</se:SvgParameter>
                            <se:SvgParameter name="stroke-dasharray">4 4</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                    <se:TextSymbolizer>
                        <!--se:Geometry>
                            <ogc:Function name="startPoint">
                                <ogc:PropertyName>geom</ogc:PropertyName>
                            </ogc:Function>
                        </se:Geometry-->
                        <se:Label>
                            <ogc:PropertyName>dmm</ogc:PropertyName>
                        </se:Label>
                        <se:Font>
                            <se:SvgParameter name="font-family">verdana</se:SvgParameter>
                            <se:SvgParameter name="font-family">Sans-Serif</se:SvgParameter>
                            <se:SvgParameter name="font-size">10</se:SvgParameter>
                        </se:Font>
                        <se:LabelPlacement>
                        <se:LinePlacement>
                            <se:PerpendicularOffset>
                                5
                            </se:PerpendicularOffset>
                        </se:LinePlacement>
                            <!--se:PointPlacement>
                                <se:Rotation>
                                    <ogc:Function name="startAngle">
                                        <ogc:PropertyName>geom</ogc:PropertyName>
                                    </ogc:Function>
                                </se:Rotation>
                            </se:PointPlacement-->
                        </se:LabelPlacement>
                        <se:Halo>
                            <se:Fill>
                                <se:SvgParameter name="fill">#FAFAFA</se:SvgParameter>
                                <se:SvgParameter name="fill-opacity">0.55</se:SvgParameter>
                            </se:Fill>
                        </se:Halo>
                        <!--se:VendorOption name="partials">true</se:VendorOption-->
                        <!--se:VendorOption name="group">yes</se:VendorOption-->
                        <se:VendorOption name="followLine">true</se:VendorOption>
                        <!--se:VendorOption name="maxDisplacement ">250</se:VendorOption-->
                    </se:TextSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>